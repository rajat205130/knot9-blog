---
layout: post
title: Understanding Stock Video Footages and Photos Licensing
description: we will offer you with the top satisfactory web sites to download 4K or ultra HD free and Royalty-loose stock films
thumb_image: /blog/images/legal-pixels-photo-534204.jpg
categories: "stock-footage-use"
tags:
- Licensing
- HD Stock Images
- Royalty Free Images
- Indian Stock Images
---
<strong>NOTE:</strong> Every site has their own terms and conditions for licensing, please check their license agreement before making the final purchase.

In your professional or personal life, there are plenty of times that you might need to use footage; clicked, shot, or composed, by someone else. But you can’t use them just like that. These assets are protected under Government Copyright Laws and you need to acquire proper licenses to use them in any capacity. Failure to do so will cause you legal implications.

<div class="blockquote">
<h4>
  The Copyright Law
</h4>
</div>
Under Copyright Law, the Indian Government offers protection to the intellect of human creation. The law establishes an exclusive and singular right to the owner, author, or creator of the ‘original’ work. The ‘work’ which can be categorized under cinematographic films, artistic works, literary works, musical, sound recording, dramatics, or Computer Software.

Besides gaining the owner an economic and moral right on his work, the Copyright Law provides him the freedom to license his work to one or more individual or a company, for a specified period of time and on certain set terms and conditions, in exchange for monetary payment.

<div class="blockquote">
<h4>
Copyright and Licencing
</h4>
</div>
Where copyright shows ownership of the intellectual property; licensing allows the owner to permit another to use his copyrighted intellectual property for a specific fee, for a certain amount of time, and under specific conditions.

The licenses offered, on various copyrighted assets usually carry:
* Worldwide use of the media.
* Optioned for multi-user.
* Perpetual without an expiration date.
* Protected from all indemnifications.


<div class="blockquote">
<h4>
Types of Licenses
</h4>
</div>
Every user has a different purpose for the stock file he requires. Therefore, different types of licenses are on offer to suit individual needs.

<strong>1. Standard Licence</strong>  A basic license that allows the purchaser the right to download and use the content as he sees fit.

A standard license allows the user to:
* Use or view the asset a set number of times for personal use.
* Use the stock file as a part of a broadcast program, multimedia production, social media, email, and mobile advertising, apps, software, e-publications, online media, etc.
* Use for labeling and packaging, billboard ads, cover art, business cards, and letterheads, live performance, etc.

The asset, however, can be used for commercial purposes in your or your client work with certain restrictions. But commercially you can not use it for resale, distribution, download, by any other person or a party other than the one who purchased it originally.

<strong>2. Enhanced License</strong> An enhanced license allows the user to have the same benefits as that of a standard license, plus:

* The user can have unlimited views or copies of the asset made.
* The image can be reproduced as wall art without further functional or creative elements.
* Can be incorporated as part of a digital template being used for sale or redistribution.
* Can be incorporated into merchandise or its physical reproduction, that is up for sale, resale, or distribution.

An enhanced license allows you to use it for commercial purpose for you and your client work with minimum restrictions, but can not use to for resale resale, download, or distribution, purpose.

It also prohibits the user from using the asset prominently.

<strong>3. Extended License</strong> Sometimes Standard licensing does not cover the need for which the content is required. In such cases, an extended license needs to be purchased.

An extended license covers all features included in the standard and enhanced licenses plus it allows the user to use the asset prominently for products or merchandise up for resale.

<strong>4. Editorial License</strong> These assets are for restrictive use only. These are images and footage that can be used only in broadcasts, films, blogs, and articles that cover news or, are specific event related.

The license is granted for:
* Unlimited views of the editorial material on the web or otherwise.
* To be used as context, in descriptive mode, alongside a story that is newsworthy or carries human interest.
* A singular purpose only, i.e, can be used in a publication or a blog post or a news story.

One should remember that:
* The license for the editorial assets can be revoked without preamble.
* You should check for the allowance of multiple distributions of the asset across mediums and channels.
* Editorial assets cannot be used for commercial work or used to assist in the sale and distribution of a product or merchandise.
* You should also check for industry or geographic usage restriction.
* Using the asset in articles that are not ‘in-context’ can attract further licensing.


<strong>5. Footage Comp License</strong> This is an additional license that allows you to use low-resolution and watermarked footage as a comp.

But this footage can be used only in rough cuts that will be reviewed, comp, sample, or test subject.

This footage can be edited but the watermark cannot be removed or altered.

Also, this footage cannot be part of or incorporated into any final material that is ready for distribution or display.

<div class="blockquote">
<h4>
In Comparison: Knot9 Licensing
</h4>
</div>
With the variety in the type of work and it's broadcasting mediums, sometimes it gets difficult to judge the scope of ones licensing need. Choosing the type of license, from so many different types, that suits your requirement, can get a little confusing too.

Knot9 understands the pain of an artist who wants to just showcase his work without legal issues marring his credibility.

That is why at Kno9, licensing is simple and easier.

<div class="blockquote">
<h4>
How Knot9 Licensing is Simpler?
</h4>
</div>
Knot9 licensing is simpler because:
* Unlike its contemporaries, the site provides a blanket license for all the assets in its library.
* The license clearly defines the different multimedia terms in an unambiguous manner.
* The Knot9 licensing terms offer non-exclusive, perpetual, and worldwide right to use the asset to the user.
* The license granted by Knot9 lists the dos and don’ts for the licensee in a simple manner. The terms are quite self-explanatory regarding the penalties accorded for misuse of the license.
* Licensing restrictions are clearly enumerated as well.
* When compared with other similar sites, jurisdiction, indemnification, intellectual property indemnification, and dispute settlement terms and location are clearly mentioned under the licensing information itself.
* Every aspect of the agreement between the licensor and the licensee like, severability, amendment, waiver, non-transferability, etc. are well defined on the Knot9 site.

No aspect of the license offered by Knot9 is hidden from its user. Buying a license for use of a multimedia asset for photography and videography has never been so easy before.

<strong>Sources:</strong>
  * [https://www.shutterstock.com/license](https://www.shutterstock.com/license)
  * [https://www.knot9.com/licensing](https://www.knot9.com/licensing)
  * [https://www.pond5.com/legal/license](https://www.pond5.com/legal/license)
  * [https://stock.adobe.com/license-terms](https://stock.adobe.com/license-terms)
  * [https://en.wikipedia.org/wiki/Copyright_law_of_India](https://en.wikipedia.org/wiki/Copyright_law_of_India)
  * [https://www.quora.com/Whats-the-difference-between-copyright-and-licensing](https://www.quora.com/Whats-the-difference-between-copyright-and-licensing)

